<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    </head>
<body>
    <div class="wrapper">
        <div class="login-form">
            <form id="logform" action="login.php" method="post">
        	  <label for="login">Логин <span id="error-auth-login" class="error"></span></label>
              <input type="text" name="login" value=""><br>
              <label for="password">Пароль<span id="error-auth-password" class="error"></span></label>
              <input type="password" name="password" value=""><br>
              <input type="submit" name="log" value="Вход">
            </form>
        </div>
        <div class="reg-form">
            <form id="regform" action="registration.php" method="post">
              <label for="login">Логин <span id="error-login" class="error"></span>
              <input type="text" name="login" value=""></label><br>
              <label for="password">Пароль <span id="error-password" class="error"></span>
              <input type="password" name="password" value=""></label><br>
              <label for="confirm_password">Проверка пароля <span id="error-confirm_password" class="error"></span>
              <input type="password" name="confirm_password" value=""></label><br>
              <label for="email">Email <span id="error-email" class="error"></span>
              <input type="text" name="email" value=""></label><br>
              <label for="name">Имя <span id="error-name" class="error"></span>
              <input type="text" name="name" value=""></label><br>
              <input type="submit" name="reg" value="Регистарция">
            </form>
        <script type="text/javascript">    regform.onsubmit = async (e) => { e.preventDefault();
    let response = await fetch('registration.php', {
      method: 'POST',
      body: new FormData(regform)
    });
    if (response.ok) { // если HTTP-статус в диапазоне 200-299
    // получаем тело ответа и выводим ошибки
    let errors = await response.json();
    var element;
    if (errors == null) {
        window.location.href = '/index.php';
    } else {
    for (key in errors) {
        element = document.getElementById('error-' + key);
        element.innerHTML = errors[key];
            }}
    } else {
    alert("Ошибка HTTP: " + response.status);
    }
    
  };    
  logform.onsubmit = async (e) => { e.preventDefault();
    let response = await fetch('login.php', {
      method: 'POST',
      body: new FormData(logform)
    });
    if (response.ok) { // если HTTP-статус в диапазоне 200-299
    // получаем тело ответа и выводим ошибки
    errors = await response.json();
    var element;
    if (errors == null) {
        window.location.href = '/index.php';
    } else {
    for (key in errors) {
        element = document.getElementById('error-auth-' + key);
        element.innerHTML = errors[key];
            }}
    } else {
    alert("Ошибка HTTP: " + response.status);
    }
    
  };</script>
        </div>
    </div>

</body>
</html>