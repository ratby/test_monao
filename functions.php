<?php
function registration() {

//поверка логина
if (!empty($_POST['login'])) {
		 if (strlen($_POST['login']) < 6) {
			$arr_Errors_Form_Registration['login'] = 'Логин слишком короткий';
		} elseif (check_base_for_uniq('login') == true) {
				$arr_Errors_Form_Registration['login'] = 'Логин уже занят. Попробуйте другой';
			}
	} else $arr_Errors_Form_Registration['login'] = 'Введите логин';

	//проверка пароля
	if (!empty($_POST['password'])) {
		 if (strlen($_POST['password']) < 6) {
			$arr_Errors_Form_Registration['password'] = 'Пароль слишком короткий';
		} elseif (!preg_match("/^[a-zA-Z0-9]+$/",$_POST['password'])) {
			$arr_Errors_Form_Registration['password'] = 'Пароль должен состоять только из букв и цифр';
		}
		
	} else {$arr_Errors_Form_Registration['password'] = 'Введите пароль';}
//проверка подтверждения пароля
	if (!empty($_POST['confirm_password'])) {
		if ($_POST['confirm_password'] != $_POST['password']) {
			$arr_Errors_Form_Registration['confirm_password'] = 'Пароли не совпадают';
				}}
	else { $arr_Errors_Form_Registration['confirm_password'] = 'Подтвердите пароль';}
//проверка email
	if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == false) {
		$arr_Errors_Form_Registration['email'] = 'Введите корректный email';
			} elseif (check_base_for_uniq('email') == true) {
				$arr_Errors_Form_Registration['email'] = 'Такой email уже зарегистрирован. Авторизируйтесь или восстановите пароль';
			}
//проверка name
	if (!empty($_POST['name'])) {
		 if (strlen($_POST['name']) < 2) {
			$arr_Errors_Form_Registration['name'] = 'Имя слишком короткое';
			}}
	else {$arr_Errors_Form_Registration['name'] = 'Введите имя';}

// Если ошибок нет добавляем пользователя, если ошибки есть, то возвращаем массив с ошибками
	if (!empty($arr_Errors_Form_Registration)) { 
		return ($arr_Errors_Form_Registration);
	} else {
		add_new_in_db(); // добавляем нового пользователя в базу данных
		add_new_cookies(free_input_data($_POST['login']));

		}

}


// получаем строку json в массив для работы
function get_json_in_arr() {
	$full_db_in_json = file_get_contents('mydb.json');
	$data = json_decode($full_db_in_json, true);
	return ($data);
}
/*после заполнения и проверки формы добавляет пользователя в БД. Можно улучшить если вынести $new_member за функцию и принимать значения функцией*/
function add_new_in_db() {
	$new_member['login'] = free_input_data($_POST['login']); //free_input_data экранирует данные перед сохранением
	$new_member['password'] = md5(md5(free_input_data($_POST['password'])));
	$new_member['email'] = free_input_data($_POST['email']);
	$new_member['name'] = free_input_data($_POST['name']);
	$new_member['user_hash'] = generate_uniq_Code();
	$full_db_in_arr = get_json_in_arr();
	$full_db_in_arr[count($full_db_in_arr)+1] = $new_member;
	$full_db_in_json_new = json_encode($full_db_in_arr);
	file_put_contents('mydb.json', $full_db_in_json_new);

}
// проверка на уникальность: если есть в базе, то возвращает true. Принимает name формы
function check_base_for_uniq($one_key_name) {
	$full_db_in_arr = get_json_in_arr();
	for ($i = 0; $i <= count($full_db_in_arr); $i++) {
		if ($full_db_in_arr[$i][$one_key_name] == free_input_data($_POST[$one_key_name])) {
			return true;
		}
	}
}


// добавляет новые cookies если успешная регистрация/авторизация
function add_new_cookies($login) {
	setcookie('login', $login, time () + 3600, "/");
	$personal_data = get_person_data_by_login($login);
	$user_hash = $personal_data['user_hash'];
	setcookie('user_hash', $user_hash, time () + 3600, "/");
}
/* экранирование. Возможно стоило бы экранировать все входящие данные до обработки и поместить в отдельный массив, а потом уже проводить валидацию */
function free_input_data($input_Data) {
	$input_Data = trim($input_Data);
	$input_Data = stripcslashes($input_Data);
	$input_Data = htmlspecialchars($input_Data);
	return $input_Data;
}
// генерация случайной hash строки
function generate_uniq_Code() {
	$length = 12;
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
            $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}
// получение данных пользователя из бд по логину
function get_person_data_by_login($login) {
	$full_db_in_arr = get_json_in_arr();
	for ($i = 0; $i <= count($full_db_in_arr); $i++) {
		if ($full_db_in_arr[$i]['login'] == $login) {
			$person_data = $full_db_in_arr[$i];
			return $person_data;
		}}
}
// проверка авторизации 1 если все ок 0 если не совпадают
function check_cookies($login_cookie, $hash_cookie) {
	$person_data = get_person_data_by_login(free_input_data($login_cookie));
	if ($person_data['user_hash'] == $hash_cookie) {
		return true;
	} else { return false; }
}
// авторизация
function authorization() {
	if (empty($_POST['login'])) { $arr_Errors_Form_auth['login'] = 'Введите логин'; 
	} //else { $arr_Errors_Form_auth['login'] = null;}
	if (empty($_POST['password'])) { $arr_Errors_Form_auth['password'] = 'Введите пароль';
			} else { 
					if (!empty($_POST['password']) and !empty($_POST['login'])) {
						$person_data = get_person_data_by_login(free_input_data($_POST['login']));
							if ($person_data['password'] == md5(md5(free_input_data($_POST['password'])))) {
							add_new_cookies(free_input_data($_POST['login']));
							} else { $arr_Errors_Form_auth['password'] = 'Не верный пароль'; }}
					}
	return $arr_Errors_Form_auth;
}
// вывод кнопки вход/регистраци и замена на приветствие для залогиненых. Если хеш куки не совпадает, то ошибка проверка не пройдена
function auth_buttons() {
	if (isset($_COOKIE['login']) and isset($_COOKIE['user_hash'])) {
	if (check_cookies($_COOKIE['login'], $_COOKIE['user_hash'])) {
	$buttons_var = "Привет, " . $_COOKIE['login'] . " " . '<a href="logout.php"><button>Выйти</button></a>';
} else {$buttons_var = "Проверка не пройдена";}
} else {$buttons_var = '<a href="singup.php"><button>Войти/Зарегистрироваться</button></a>';}
return $buttons_var;
}
